package top.jimc.ehcache.service;

import top.jimc.ehcache.po.User;

/**
 * @author Jimc.
 * @since 2018/9/21.
 */
public interface EhcacheService {

    String getTimestamp(String param);

    String getDataFromDB(String key);

    void removeDataAtDB(String key);

    String refreshData(String key);


    User findUserById(String userId);

    void removeUserById(String userId);

    void removeAllUser();

}
