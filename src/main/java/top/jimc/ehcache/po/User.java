package top.jimc.ehcache.po;

import java.io.Serializable;

/**
 * @author Jimc.
 * @since 2018/9/21.
 */
public class User implements Serializable {// 注意：如果需要将缓存写入到磁盘中，必须序列化
    private static final long serialVersionUID = -4837522189706248787L;
    private String userId;
    private String username;

    public User() {
    }

    public User(String userId, String username) {
        this.userId = userId;
        this.username = username;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
