import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import top.jimc.ehcache.service.EhcacheService;

/**
 * @author Jimc.
 * @since 2018/9/21.
 */
public class EhcacheServiceTest extends BaseJunit4Test {

    @Autowired
    private EhcacheService ehcacheService;

    @Test
    public void testTimestamp() throws InterruptedException {
        System.out.println("第一次调用：" + ehcacheService.getTimestamp("param"));
        Thread.sleep(4000);
        System.out.println("再过4秒之后调用：" + ehcacheService.getTimestamp("param"));
        Thread.sleep(11000);
        System.out.println("再过11秒之后调用：" + ehcacheService.getTimestamp("param"));
    }

    @Test
    public void testDataCache() {
        String key = "LiSi";
        String value = ehcacheService.getDataFromDB(key);// 模拟从数据库中获取数据
        System.out.println(value);
        value = ehcacheService.getDataFromDB(key);// 从缓存中获取数据，所以不执行该方法体
        System.out.println(value);
        ehcacheService.removeDataAtDB(key);// 从数据库中删除数据
        value = ehcacheService.getDataFromDB(key);  // 再次从数据库中获取数据（缓存数据删除了，所以要重新获取，执行方法体）
        System.out.println(value);
    }

    @Test
    public void testDataPut() {
        String key = "WangWu";
        String value = ehcacheService.refreshData(key);// 模拟从数据库中加载数据
        System.out.println(value);
        value = ehcacheService.getDataFromDB(key);// 从缓存中获取数据，所以不执行该方法体
        System.out.println(value);

        value = ehcacheService.refreshData(key);// 再次模拟从数据库中加载数据，此时会执行方法体
        System.out.println(value);
        value = ehcacheService.getDataFromDB(key);// 从缓存中获取数据，所以不执行该方法体
        System.out.println(value);

    }


    @Test
    public void testFindById() {
        System.out.println(ehcacheService.findUserById("1"));// 先模拟从数据库中查询数据
        System.out.println(ehcacheService.findUserById("1"));// 从缓存中取数据，不会执行方法体
    }

    @Test
    public void testRemoveUserById() {
        System.out.println(ehcacheService.findUserById("1"));// 先添加到缓存

        ehcacheService.removeUserById("1");// 再删除

        System.out.println(ehcacheService.findUserById("1")); // 再查询，如果不存在会执行方法体
    }

    @Test
    public void testRemoveAllUser() {
        // 先模拟从数据库中查询数据
        System.out.println(ehcacheService.findUserById("1"));
        System.out.println(ehcacheService.findUserById("2"));

        ehcacheService.removeAllUser();// 清除cacheTest中全部缓存

        // 重新模拟从数据库中查询数据，此时执行了方法体，证明缓存中的数据已被清除
        System.out.println(ehcacheService.findUserById("1"));
        System.out.println(ehcacheService.findUserById("2"));
    }


}
